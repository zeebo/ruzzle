package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strings"
)

var scores = map[byte]int{
	'a': 1,
	'c': 4,
	'd': 2,
	'e': 1,
	'h': 4,
	'i': 1,
	'l': 1,
	'm': 3,
	'n': 1,
	'o': 1,
	'p': 4,
	'r': 1,
	's': 1,
	't': 1,
	'u': 2,
	'w': 4,
	'y': 4,
	'f': 4,
	'b': 4,
	'g': 3,
	'j': 0,
	'k': 0,
	'q': 0,
	'v': 4,
	'x': 0,
	'z': 10,
}

func getScore(letter byte) int {
	val := scores[letter]
	if val == 0 {
		panic(string(letter))
	}
	return val
}

func fatal(err error) {
	if err != nil {
		panic(err)
	}
}

func parse(input string) (b board) {
	var tn int
	var t tile
	for i := 0; i < len(input); i++ {
		t.letter = input[i]

		sm := int8(1)
		for i+1 < len(input) && input[i+1] == '!' {
			sm++
			i++
		}
		t.sm = sm

		wm := int8(1)
		for i+1 < len(input) && input[i+1] == '*' {
			wm++
			i++
		}
		t.wm = wm

		b[tn] = t
		tn++
	}

	return b
}

func loadWords() (words []string) {
	fh, err := os.Open("words.txt")
	fatal(err)
	defer fh.Close()

	scanner := bufio.NewScanner(fh)
	for scanner.Scan() {
		text := strings.TrimSpace(scanner.Text())
		if len(text) > 0 {
			words = append(words, text)
		}
	}
	fatal(scanner.Err())
	return words
}

type position int8

func (p position) xy() (int8, int8) {
	return int8(p & 3), int8(p >> 2 & 3)
}

// 0  1  2  3
// 4  5  6  7
// 8  9 10 11
//12 13 14 15

var neighbors = [16][]position{
	0:  {1, 4, 5},
	1:  {0, 2, 4, 5, 6},
	2:  {1, 3, 5, 6, 7},
	3:  {2, 6, 7},
	4:  {0, 1, 5, 8, 9},
	5:  {0, 1, 2, 4, 6, 8, 9, 10},
	6:  {1, 2, 3, 5, 7, 9, 10, 11},
	7:  {2, 3, 6, 10, 11},
	8:  {4, 5, 9, 12, 13},
	9:  {4, 5, 6, 8, 10, 12, 13, 14},
	10: {5, 6, 7, 9, 11, 13, 14, 15},
	11: {6, 7, 10, 14, 15},
	12: {8, 9, 13},
	13: {8, 9, 10, 12, 14},
	14: {9, 10, 11, 13, 15},
	15: {10, 11, 14},
}

type tile struct {
	letter byte
	sm     int8
	wm     int8
}

func (t tile) String() string {
	if t.sm > 1 {
		return fmt.Sprintf("%s% -2s", string(t.letter),
			strings.Repeat("!", int(t.sm)-1))
	}
	if t.wm > 1 {
		return fmt.Sprintf("%s% -2s", string(t.letter),
			strings.Repeat("*", int(t.wm)-1))
	}
	return fmt.Sprintf("%s  ", string(t.letter))
}

type board [16]tile

func (b board) String() string {
	out := ""
	for i := 0; i < 4; i++ {
		out += fmt.Sprintln(b[i*4 : (i+1)*4])
	}
	return out
}

func (b board) Score(word string) int {
	wl := len(word)
	first, word := word[0], word[1:]
	best_score := -1
	for i, t := range b {
		if t.letter != first {
			continue
		}
		used := [16]bool{}
		used[i] = true
		start_score := getScore(t.letter) * int(t.sm)
		cand_score := b.score(wl, word, used, t.wm, position(i), start_score)
		if cand_score > best_score {
			best_score = cand_score
		}
	}
	return best_score
}

func bonus(wl int) int {
	switch wl {
	case 9:
		return 25
	case 8:
		return 20
	case 7:
		return 15
	case 6:
		return 10
	case 5:
		return 5
	}
	return 0
}

func (b board) score(wl int, word string, used [16]bool, wm int8, cur position,
	score int) int {

	// x, y := cur.xy()
	// fmt.Println(x, y, word, wm, score)

	if len(word) == 0 {
		return score*int(wm) + bonus(wl)
	}

	used[cur] = true
	first, word := word[0], word[1:]
	best_score := -1
	for _, n := range neighbors[cur] {
		if used[n] {
			continue
		}

		t := b[n]
		if t.letter != first {
			continue
		}

		letter_score := getScore(t.letter) * int(t.sm)
		cand_score := b.score(wl, word, used, wm*t.wm, n, score+letter_score)
		if cand_score > best_score {
			best_score = cand_score
		}
	}

	return best_score
}

type wordScore struct {
	word  string
	score int
}

type ws []wordScore

func (w ws) Len() int           { return len(w) }
func (w ws) Less(i, j int) bool { return w[i].score > w[j].score }
func (w ws) Swap(i, j int)      { w[i], w[j] = w[j], w[i] }

func main() {
	b := parse(os.Args[1])
	fmt.Println(b)

	words := loadWords()
	fmt.Printf("loaded %d words\n", len(words))

	scores := make([]wordScore, 0, len(words))
	for _, word := range words {
		scores = append(scores, wordScore{
			word:  word,
			score: b.Score(word),
		})
	}
	sort.Sort(ws(scores))
	for i := 0; i < 50; i++ {
		fmt.Println(scores[i])
	}
}
